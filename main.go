package main

import (
	"fmt"
	_ "github.com/mattn/go-oci8"
	"gitlab.com/rachata_appworks/restruct/adapters/repository/oracleConnecting"
	"gitlab.com/rachata_appworks/restruct/di"
	"gitlab.com/rachata_appworks/restruct/domain/app"
	"gitlab.com/rachata_appworks/restruct/service/logCleaner"
)

var prop *app.Properties

func bootstrap() error {
	//init config
	prop = new(app.Properties)
	v, err := app.NewConf()
	if err != nil {
		return fmt.Errorf("App propertie not found %v", err)
	}
	prop.Viper = v

	//init oracle db
	oracleConnecting.Properties(prop)
	oc := oracleConnecting.NewRepository()
	db, err := oc.OpenConn()
	if err != nil {
		panic(err)
	}
	prop.Oracle = db
	db.SetMaxIdleConns(5)

	di.Inject(prop)
	return nil
}

func main() {
	bootstrap()
	logCleaner.Clean()
	//repo := logCleaning.NewRepository()
	//repo.InsertFakeLogsCurrentDate()
	//repo.InsertFakeLogsExpiredDate()
}


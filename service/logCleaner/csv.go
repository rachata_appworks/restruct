package logCleaner

import (
	"encoding/csv"
	"gitlab.com/rachata_appworks/restruct/domain/auditLog"
	"os"
	"strconv"
)

func writeCsvFiles(oldLogs []auditLog.AuditLogDetail, currentRow int) error{
	rowsPerRound := prop.Viper.GetInt("customable.csv.row")
	csvFileName := prop.Viper.GetString("customable.csv.name")

	csvContents := [][]string{}
	csvHeader := []string{
		"ACCOUNT_ID",
		"FULL_NAME",
		"EMAIL",
		"PHONE",
		"TOKEN",
		"CREATE_BY",
		"CREATE_DATE",
		"UPDATE_DATE",
		"UPDATE_BY"}
	csvContents = append(csvContents, csvHeader)
	for _, v := range oldLogs {
		eachLog := []string{
			v.ACCOUNT_ID,
			v.FULL_NAME,
			v.EMAIL,
			v.PHONE,
			v.TOKEN,
			v.CREATE_BY,
			v.CREATE_DATE.String(),
			v.UPDATE_DATE.String(),
			v.UPDATE_BY}
		csvContents = append(csvContents, eachLog)
	}
	err := writeCsvFile(currentRow, rowsPerRound, csvFileName, csvContents)
	if err != nil {
		return err
	}
	return nil
}

func writeCsvFile(currentRow, rowsPerRound int, csvFileName string, csvContents [][]string) error{


	file, err := os.Create(csvFileName + strconv.Itoa(currentRow) + "to" + strconv.Itoa(currentRow+rowsPerRound-1) + ".csv")
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()
	for _, value := range csvContents {
		err := writer.Write(value)
		if err != nil {
			return err
		}
	}
	println("write csv complete")
	return nil

}

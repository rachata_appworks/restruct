package logCleaner

import (
	"bytes"
	"io"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"
)

func createFile(checkpointName string) error{
	var _, err = os.Stat(checkpointName)

	if os.IsNotExist(err) {
		var file, err = os.Create(checkpointName)
		if err != nil {
			return err
		}
		defer file.Close()
	}
	return nil
}

func writeCheckpoint(checkpointName, currentRow, totalRows string, csvNameList []string) error{
	err := createFile(checkpointName)
	if err != nil {
		return err
	}
	file, err := os.OpenFile(checkpointName, os.O_RDWR, 0644) // Open file using READ & WRITE permission.
	if err != nil {
		return err
	}
	defer file.Close()

	var strCsvNameList string
	for _, v := range csvNameList {
		strCsvNameList += v
		strCsvNameList += " "
	}
	strCsvNameList = trimLastChar(strCsvNameList)
	_, err = file.WriteString(currentRow + " " + totalRows + " " + strCsvNameList)
	if err != nil {
		return err
	}

	err = file.Sync()
	if err != nil {
		return err
	}
	return nil
}

func readFile(checkpointname string) (currentRow, totalRows int, csvNameList []string, err error) {
	// Open file for reading.
	file, err := os.OpenFile(checkpointname, os.O_RDWR, 0644)
	if err != nil {
		return 0, 0, nil, err
	}
	defer file.Close()

	// Read file, line by line
	var text = make([]byte, 1024)
	for {
		_, err = file.Read(text)
		// Break if finally arrived at end of file
		if err == io.EOF {
			break
		}
		// Break if error occured
		if err != nil {
			return 0, 0, nil, err
		}
	}
	bytedata := bytes.Trim(text, "\x00")
	splited := strings.Split(string(bytedata), " ")
	currentRow, err = strconv.Atoi(splited[0])
	if err != nil {
		file.Close()
		err = deleteFile(nil) // if that var cant convert to int so it's incorrect data(blank, letters, etc) so i delete
		return 0, 0, nil, err
	}
	totalRows, err = strconv.Atoi(splited[1])
	if err != nil || totalRows == 0{
		file.Close()
		err = deleteFile(nil)
		return 0, 0, nil, err
	}
	var prepareNameList []string
	for i := 2; i < len(splited); i++ {
		prepareNameList = append(prepareNameList, strings.TrimSpace(splited[i]))
	}
	println("rows read from blank file : ", currentRow, totalRows, prepareNameList)
	return currentRow, totalRows, prepareNameList, nil
}

func deleteFile(csvNameList []string) error{
	checkpointName := prop.Viper.GetString("customable.checkpointname")
	for _, v := range csvNameList {
		err := os.Remove(v)
		if err != nil {
			return err
		}
	}
	err := os.Remove(checkpointName)
	if err != nil {
		return err
	}
	println("Files : DELETED")
	return nil
}

func trimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}

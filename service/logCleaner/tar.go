package logCleaner

import (
	"archive/tar"
	"compress/gzip"
	"io"
	"os"
	"time"
)

func toTar(csvNameList []string) error {
	tarName := prop.Viper.GetString("customable.tar.name")
	storagePath := prop.Viper.GetString("customable.tar.storagepath")
	
	fileName := tarName + time.Now().Format("02-01-2006") + ".tar.gz"
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()
	gw := gzip.NewWriter(file)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()
	for i := range csvNameList {
		if err := addFileToTar(tw, csvNameList[i]); err != nil {
			return err
		}
	}
	println("before move")
	moveTarLocation(fileName, storagePath)
	
	return nil
}

func addFileToTar(tw *tar.Writer, path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	if stat, err := file.Stat(); err == nil {
		header := new(tar.Header)
		header.Name = path
		header.Size = stat.Size()
		header.Mode = int64(stat.Mode())
		header.ModTime = stat.ModTime()
		if err := tw.WriteHeader(header); err != nil {
			return err
		}
		if _, err := io.Copy(tw, file); err != nil {
			return err
		}
	}
	return nil
}

func moveTarLocation(fileName, storagePath string) error{
	println("inside move")
	oldLocation := fileName
	newLocation := storagePath + "/" + fileName
	err := os.Rename(oldLocation, newLocation)
	if err != nil {
		return err
	}
	return nil
}
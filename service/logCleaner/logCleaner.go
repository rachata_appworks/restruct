package logCleaner

import (
	"fmt"
	_ "github.com/mattn/go-oci8"
	"gitlab.com/rachata_appworks/restruct/adapters/repository/logCleaning"
	"log"
	"os"
	"strconv"
	"time"
)

func Clean() error {
	checkpointName := prop.Viper.GetString("customable.checkpointname")
	csvDuration := prop.Viper.GetInt("customable.duration.csvday")
	expiredDate := time.Now().AddDate(0, 0, -csvDuration)
	var currentRow int
	var totalRows int
	var csvNameList []string
	_, err := os.Stat(checkpointName)
	if os.IsNotExist(err) {
		currentRow, totalRows, err = getFirstEndRow(expiredDate)
		if err != nil {
			fmt.Println("error at Get 1st row : ",err)
		}
	} else {
		currentRow, totalRows, csvNameList, err = loadCheckPoint(expiredDate)
		if err != nil {
			fmt.Println("error at load check point : ",err)
		}
	}
	if totalRows > 0 {
		err = createCsvFiles(currentRow, totalRows, csvNameList)
		if err != nil {
			log.Print("error at createCsvFiles",err)
		}
	} else {
		fmt.Println("didnt found any old log\nno job to do here")
	}
	return nil
}

func getFirstEndRow(expiredDate time.Time) ( currentRow, totalRows int, err error ){
	repo := logCleaning.NewRepository()
	totalRows, err = repo.CountOldLogs(expiredDate)
	checkpointName := prop.Viper.GetString("customable.checkpointname")
	if err != nil {
		return 0, 0, err
	}
	currentRow = 1
	err = writeCheckpoint(checkpointName,strconv.Itoa(currentRow), strconv.Itoa(totalRows), nil)
	if err != nil {
		return 0, 0, err
	}
	return currentRow, totalRows, nil
}

func loadCheckPoint(expiredDate time.Time) ( currentRow, totalRows int, csvNameList []string, err error ){
	checkpointName := prop.Viper.GetString("customable.checkpointname")
	currentRow, totalRows, csvNameList, err = readFile(checkpointName)
	if err != nil {
		return 0, 0, nil, err
	}
	if !(currentRow == 0 && totalRows == 0 && csvNameList != nil) {
		currentRow = currentRow
		totalRows = totalRows
		csvNameList = csvNameList
	} else {
		currentRow, totalRows, _ = getFirstEndRow(expiredDate)
	}
	return currentRow, totalRows, csvNameList, nil
}

func createCsvFiles(currentRow ,totalRows int, csvNameList []string) error{
	repo := logCleaning.NewRepository()
	csvDuration := prop.Viper.GetInt("customable.duration.csvday")
	rowsPerRound := prop.Viper.GetInt("customable.csv.row")
	csvFileName := prop.Viper.GetString("customable.csv.name")
	checkpointName := prop.Viper.GetString("customable.checkpointname")
	for {
		//if currentRow > 300 { //breaker
		//	break
		//}
		oldLogs, err := repo.GetOldLogs(csvDuration, rowsPerRound, currentRow)
		if err != nil {
			return err
		}
		writeCsvFiles(oldLogs, currentRow)
		csvNameList = append(csvNameList, csvFileName+strconv.Itoa(currentRow)+"to"+strconv.Itoa(currentRow+rowsPerRound-1)+".csv")
		currentRow += rowsPerRound
		err = writeCheckpoint(checkpointName, strconv.Itoa(currentRow), strconv.Itoa(totalRows), csvNameList)
		if err != nil {
			return err
		}
		if currentRow > totalRows {
			toTar(csvNameList)
			deleteFile(csvNameList)
			repo.DeleteOldLogs(csvDuration)
			break
		}
	}
	return nil
}
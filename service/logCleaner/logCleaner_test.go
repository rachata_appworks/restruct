// +build integration

package logCleaner

import (
	"github.com/spf13/viper"
	"gitlab.com/rachata_appworks/restruct/domain/app"
	"gitlab.com/rachata_appworks/restruct/domain/auditLog"
	"io/ioutil"
	"os"
	"strconv"
	"testing"
	"time"
)

func initConf() (*viper.Viper, error) {
	app.Path = "../../"
	c, err := app.NewConf()
	if err != nil {
		return nil, err
	}
	return c, nil
}

func Test_service_csv_writer_must_have_csv_file(t *testing.T) {
	_, err := initConf()
	if err != nil {
		t.Error(err)
	}
	rowsPerRound := 200
	csvFileName := "csv_old_log"
	//rowsPerRound := viper.GetInt("customable.csv.row")
	//csvFileName := viper.GetString("customable.csv.name")

	var fakeLogs []auditLog.AuditLogDetail
	var p auditLog.AuditLogDetail
	for i := 0; i < rowsPerRound; i++ {
		p.ACCOUNT_ID = "MOCK0000ACCID"
		p.FULL_NAME = "mockname " + strconv.Itoa(i)
		p.EMAIL = "mock@email.com"
		p.PHONE = "MOCK080000"
		p.TOKEN = "T0l<3N_MOCl<"
		p.CREATE_BY = "TEST_SYSTEM"
		p.CREATE_DATE = time.Now()
		p.UPDATE_DATE = time.Now()
		p.UPDATE_BY = "TEST_SYSTEM"
		fakeLogs = append(fakeLogs, p)
	}

	csvContents := [][]string{}
	csvHeader := []string{
		"ACCOUNT_ID",
		"FULL_NAME",
		"EMAIL",
		"PHONE",
		"TOKEN",
		"CREATE_BY",
		"CREATE_DATE",
		"UPDATE_DATE",
		"UPDATE_BY"}
	csvContents = append(csvContents, csvHeader)
	for _, v := range fakeLogs {
		eachLog := []string{
			v.ACCOUNT_ID,
			v.FULL_NAME,
			v.EMAIL,
			v.PHONE,
			v.TOKEN,
			v.CREATE_BY,
			v.CREATE_DATE.String(),
			v.UPDATE_DATE.String(),
			v.UPDATE_BY}
		csvContents = append(csvContents, eachLog)
	}
	err = writeCsvFile(1, rowsPerRound, csvFileName, csvContents)
	if err != nil {
		t.Error(err)
	}

	files, err := ioutil.ReadDir("./")
	if err != nil {
		t.Error(err)
	}
	expected := csvFileName + strconv.Itoa(1) + "to" + strconv.Itoa(rowsPerRound) + ".csv"
	found := searchCsvFile(files, expected)
	if found != true {
		t.Error("CSV FILE NOT FOUND")
	}
	err = os.Remove(expected)
	if err != nil {
		t.Error(err)
	}
	println("FAKE CSV File : DELETED")
}

func searchCsvFile(files []os.FileInfo, expected string) bool{
	for _, f := range files {
		if f.Name() == expected {
			return true
		}
	}
	return false
}

func Test_file_utility(t *testing.T){
	checkpointName := "checkpoint.txt"
	err := createFile(checkpointName)
	if err != nil {
		t.Error(err)
	}

	currentRow := 1
	totalRows := 200
	err = writeCheckpoint(checkpointName, strconv.Itoa(currentRow), strconv.Itoa(totalRows), nil)
	if err != nil {
		t.Error(err)
	}

	readCurrentRow, readTotalRows, _, err := readFile(checkpointName)
	if err != nil {
		t.Error(err)
	}
	if readCurrentRow != currentRow{
		t.Error(err)
	}
	if readTotalRows != totalRows {
		t.Error(err)
	}

	err = os.Remove(checkpointName)
}

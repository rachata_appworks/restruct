package auditLog

import "time"

type IRepository interface {
	CountOldLogs(time time.Time) (int, error)
	GetOldLogs(int, int, int) ([]AuditLogDetail, error)
	DeleteOldLogs(int) error
	//FakeInsertOldLogs() error
	InsertFakeLogsCurrentDate() error
	InsertFakeLogsExpiredDate() error
}

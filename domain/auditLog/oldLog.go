package auditLog

import "time"

type AuditLogDetail struct {
	ID          int
	ACCOUNT_ID  string
	FULL_NAME   string
	EMAIL       string
	PHONE       string
	TOKEN       string
	CREATE_BY   string
	CREATE_DATE time.Time
	UPDATE_DATE time.Time
	UPDATE_BY   string
	RNUM        int
}

package datasource

import "database/sql"

type IOracleConn interface {
	OpenConn() (*sql.DB, error)
}

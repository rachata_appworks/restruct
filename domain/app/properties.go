package app

import (
	"database/sql"
	"github.com/spf13/viper"
)

type Properties struct {
	Viper  *viper.Viper
	Oracle *sql.DB
}

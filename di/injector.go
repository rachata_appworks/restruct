package di

import (
	"gitlab.com/rachata_appworks/restruct/adapters/repository/logCleaning"
	"gitlab.com/rachata_appworks/restruct/domain/app"
	"gitlab.com/rachata_appworks/restruct/service/logCleaner"
)

func Inject(ap *app.Properties) {
	injectDomain(ap)
	injectAdapter(ap)
}

func injectDomain(ap *app.Properties) {
	logCleaning.Properties(ap)
	logCleaner.Properties(ap)
}

func injectAdapter(ap *app.Properties) {
	logCleaning.Properties(ap)
	logCleaner.Properties(ap)
}

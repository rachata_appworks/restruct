package oracleConnecting

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"gitlab.com/rachata_appworks/restruct/domain/datasource"
)

func NewRepository() datasource.IOracleConn {
	return &repository{}
}

type repository struct {
}

func (a repository) OpenConn() (*sql.DB, error) {
	driver := prop.Viper.GetString("datasources.oracle.driver")
	host := prop.Viper.GetString("datasources.oracle.host")
	port := prop.Viper.GetInt("datasources.oracle.port")
	pwd := prop.Viper.GetString("datasources.oracle.password")
	user := prop.Viper.GetString("datasources.oracle.username")
	sid := prop.Viper.GetString("datasources.oracle.sid")
	uri := fmt.Sprintf("%s/%s@%s:%d/%s", user, pwd, host, port, sid)
	db_, err := sql.Open(driver, uri)
	if err != nil {
		return nil, err
	}
	err = db_.Ping()
	if err != nil {
		return nil, err
	}
	return db_, nil
}

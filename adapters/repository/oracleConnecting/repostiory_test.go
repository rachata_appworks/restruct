// +build !prod,!dev

package oracleConnecting

import (
	"github.com/spf13/viper"
	"gitlab.com/rachata_appworks/restruct/domain/app"
	"testing"
)

func initConf() (*viper.Viper, error) {
	app.Path = "../../../"
	c, err := app.NewConf()
	if err != nil {
		return nil, err
	}
	return c, nil
}

func Test_initConf(t *testing.T) {
	viper, err := initConf()
	if err != nil {
		t.Error(err)
	}
	var app app.Properties
	app.Viper = viper
	Properties(&app)
}

func Test_repository_OpenConn_must_pass(t *testing.T) {
	repo := NewRepository()
	db, err := repo.OpenConn()
	if err != nil {
		t.Error(err)
	}
	if err := db.Ping(); err != nil {
		t.Error(err)
	}
}

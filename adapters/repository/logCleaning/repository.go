package logCleaning

import (
	"database/sql"
	"fmt"
	"gitlab.com/rachata_appworks/restruct/domain/auditLog"
	"time"
)

var oc *sql.DB

const (
	findDataPack = `SELECT * FROM
    	( SELECT a.*, ROWNUM rnum FROM
        	( SELECT * FROM DLT_AUDIT_LOG WHERE CREATE_DATE < :1 ) a
      		WHERE ROWNUM <= :2 )
		WHERE rnum  >= :3`

	countOldAuditLog = `SELECT COUNT(*) FROM DLT_AUDIT_LOG WHERE CREATE_DATE < :1`

	deleteOldData = `DELETE FROM DLT_AUDIT_LOG WHERE CREATE_DATE < :1`

	insertFakeLogs = `INSERT INTO DLT_AUDIT_LOG(
			ACCOUNT_ID,
			FULL_NAME,
			EMAIL,
			PHONE,
			TOKEN,
			CREATE_BY,
			CREATE_DATE,
			UPDATE_DATE,
			UPDATE_BY
			)values(:1,:2,:3,:4,:5,:6,:7,:8,:9)`
)

func NewRepository() auditLog.IRepository {
	return &repository{}
}

type repository struct {
}

func (a *repository) CountOldLogs(expiredDate time.Time) (int, error) {
	var count int
	stmt, err := oc.Prepare(countOldAuditLog)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	r, err := stmt.Query(expiredDate)
	if err != nil {
		return 0, err
	}
	for r.Next() {
		if err := r.Scan(&count); err != nil {
			return 0, err
		}
	}
	return count, nil
}

func (a *repository) GetOldLogs(csvDuration, rowsPerRound, currentRow int) ([]auditLog.AuditLogDetail, error) {

	expiredDate := time.Now().AddDate(0, 0, -csvDuration)
	oldLogs:= make([]auditLog.AuditLogDetail,0)
	stmt, err := oc.Prepare(findDataPack)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query(expiredDate, currentRow+rowsPerRound-1, currentRow)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var oldLog auditLog.AuditLogDetail
		err = rows.Scan(&oldLog.ID, &oldLog.ACCOUNT_ID, &oldLog.FULL_NAME, &oldLog.EMAIL, &oldLog.PHONE, &oldLog.TOKEN, &oldLog.CREATE_BY, &oldLog.CREATE_DATE, &oldLog.UPDATE_DATE, &oldLog.UPDATE_BY, &oldLog.RNUM)
		if err != nil {
			return nil, err
		}
		oldLogs = append(oldLogs, oldLog)
	}
	return oldLogs, nil
}

func (a *repository) DeleteOldLogs(csvDuration int) error {
	//csvDuration := prop.Viper.GetInt("customable.duration.csvday")
	expiredDate := time.Now().AddDate(0, 0, -csvDuration)
	stmt, err := oc.Prepare(deleteOldData)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(expiredDate)
	if err != nil {
		return err
	}
	return nil
}

func (a *repository) InsertFakeLogsCurrentDate() error{
	currentDate := time.Now()
	var freshNames = []string{"fresh 1", "fresh 2", "fresh 3", "fresh 4", "fresh 5"}
	for i, v := range freshNames {
		fmt.Printf("insert fresh fake logs round : %d\n", i+1)
		for totalRows := 0; totalRows < 2; totalRows++ {
			statement, err := oc.Prepare(insertFakeLogs)
			if err != nil {
				return err
			}
			_, err = statement.Exec("ACCID0000TEST", "testname "+v, "test@test.com", "080000000", "@@!!$$", "SYSTEM001", currentDate, currentDate, "SYSTEM001")
			if err != nil {
				return err
			}
			statement.Close()
		}
	}
	return nil
}

func (a *repository) InsertFakeLogsExpiredDate() error{
	oldDate := time.Now().AddDate(0, 0, -90)
	var expiringNames = []string{"expiring 1", "expiring 2", "expiring 3", "expiring 4"}
	for i, v := range expiringNames {
		fmt.Printf("insert expire fake logs round : %d\n", i+1)
		for totalRows := 0; totalRows < 100; totalRows++ {
			statement, err := oc.Prepare(insertFakeLogs)
			if err != nil {
				return err
			}
			_, err = statement.Exec("ACCID0000TEST", "testname "+v, "test@test.com", "080000000", "@@!!$$", "SYSTEM001", oldDate, oldDate, "SYSTEM001")
			if err != nil {
				return err
			}
			statement.Close()
		}
	}
	return nil
}

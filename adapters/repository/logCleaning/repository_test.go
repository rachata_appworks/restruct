// +build integration

package logCleaning

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-oci8"
	"github.com/spf13/viper"
	"gitlab.com/rachata_appworks/restruct/domain/app"
	"testing"
	"time"
)

func initConf() (*viper.Viper, error) {
	app.Path = "../../../"
	c, err := app.NewConf()
	if err != nil {
		return nil, err
	}
	return c, nil
}

func Test_initDB(t *testing.T) {
	viper, err := initConf()
	if err != nil {
		t.Error(err)
	}
	driver := viper.GetString("datasources.oracle.driver")
	host := viper.GetString("datasources.oracle.host")
	port := viper.GetInt("datasources.oracle.port")
	pwd := viper.GetString("datasources.oracle.password")
	user := viper.GetString("datasources.oracle.username")
	sid := viper.GetString("datasources.oracle.sid")
	uri := fmt.Sprintf("%s/%s@%s:%d/%s", user, pwd, host, port, sid)
	db_, err := sql.Open(driver, uri)
	if err != nil {
		println("error at db conn : ",db_)
		panic(err)
	}
	oc = db_
}

func Test_repository_insert_fake_log_must_pass(t *testing.T){
	adapter := NewRepository()
	if err := adapter.InsertFakeLogsCurrentDate(); err != nil {
		t.Log("error at current date")
		t.Error(err)
	}
	if err := adapter.InsertFakeLogsExpiredDate(); err != nil {
		t.Log("error at expired date")
		t.Error(err)
	}
}

func Test_repository_get_count_must_pass(t *testing.T) {
	viper, err := initConf()
	if err != nil {
		t.Error(err)
	}
	csvDuration := viper.GetInt("customable.duration.csvday")
	expiredDate := time.Now().AddDate(0, 0, -csvDuration)

	adapter := NewRepository()
	actual, err := adapter.CountOldLogs(expiredDate)
	if err != nil {
		t.Error(err.Error())
	}
	if actual < 0 {
		t.Error("expect >0 0 but actual : ",err.Error())
	}
}

func Test_repository_get_data_pack_must_pass(t *testing.T) {
	viper, err := initConf()
	if err != nil {
		t.Error(err)
	}
	csvDuration := viper.GetInt("customable.duration.csvday")
	rowsPerRound := viper.GetInt("customable.csv.row")

	adapter := NewRepository()
	if _, err := adapter.GetOldLogs(csvDuration, rowsPerRound, 1); err != nil {
		t.Error(err)
	}
}

func Test_repository_delete_old_data_in_database_must_equal_to_zero(t *testing.T) {
	viper, err := initConf()
	if err != nil {
		t.Error(err)
	}
	csvDuration := viper.GetInt("customable.duration.csvday")
	expiredDate := time.Now().AddDate(0, 0, -csvDuration)

	adapter := NewRepository()
	if err := adapter.DeleteOldLogs(csvDuration); err != nil {
		t.Error(err)
	}

	count, err := adapter.CountOldLogs(expiredDate)
	if err != nil {
		t.Error(err)
	}
	expected := 0
	actual := count
	if actual != expected {
		t.Errorf("expected  %d but actual  %d", expected, actual)
	}
}
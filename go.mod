module gitlab.com/rachata_appworks/restruct

go 1.14

require (
	github.com/mattn/go-oci8 v0.1.0
	github.com/spf13/viper v1.7.0
	gopkg.in/go-playground/assert.v1 v1.2.1
)
